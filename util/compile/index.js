"use strict"

const fs = require("fs")
const path = require("path")
const cp = require("child_process")
const O = require("../js-util")
const assert = require("../assert")

const cwd = __dirname
const proj_dir = path.join(cwd, "../..")
const compiler_dir = proj_dir
const src_dir = path.join(proj_dir, "src")
const exe_file = path.join(proj_dir, "main")

const src_exts = [
  "c",
  "cc",
  "cpp",
]

const main = () => {
  const src_files = get_src_files(compiler_dir, 1)

  const res = cp.spawnSync("cc", [
    `-O3`,
    `-o`, exe_file,
    ...src_files,
    `-std=c++20`,
    `-lstdc++`,
    `-lm`,
    `-D`, `FLAG_LINUX`,
    `-D`, `COMPILER_DIR="${compiler_dir}"`,
    `-D`, `TEST=5 + 9`,
  ], {
    cwd: compiler_dir,
  })

  const stdout = res.stdout.toString()
  const stderr = res.stderr.toString()
  const out = [stdout, stderr]
    .map(a => a.trim())
    .filter(a => a.length !== 0)
    .join("\n")

  if(out.length !== 0) log(out)
}

const get_src_files = (dir=null, add_dot=0) => {
  const src_files = []
  const stack = [src_dir]

  while(stack.length !== 0){
    const dir = stack.pop()
    const names = fs.readdirSync(dir)

    for(const name of names){
      const pth = path.join(dir, name)
      const stat = fs.statSync(pth)

      if(stat.isDirectory()){
        stack.push(pth)
        continue
      }

      if(stat.isFile()){
        const ext0 = path.parse(name).ext
        if(ext0.length === 0) continue
        
        assert(ext0[0] === ".")
        const ext = ext0.slice(1)

        if(!src_exts.includes(ext))
          continue

        src_files.push(pth)
        continue
      }
    }
  }

  assert(src_files.length !== 0)

  if(dir === null) return src_files

  return get_src_files().map(pth => {
    let rel = path.relative(dir, pth)
    if(add_dot) rel = `./${rel}`
    return rel
  })
}

main()