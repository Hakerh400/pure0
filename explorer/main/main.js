"use strict"

const fs = require("fs")
const path = require("path")
const assert = require("assert")

const cwd = __dirname
const proj_dir = path.join(cwd, "../..")
const data_dir = path.join(proj_dir, "data")
const info_file = path.join(data_dir, "info.txt")

const version = 1
const search_max_res_num = 35
const display_pair_parens_in_info = 1

const page_types = [
  "exprs",
  "cmds",
]

const attribs = [
  "name",
  "pair",
  "cert",
  "ctor",
  "param",
  "parametrized",
  "cmd",
]

const version_str = `v${version}`
const attribs_num = attribs.length

const exprs = []
const cmds = []
const mp_name_e = new Map();

let state = null
let div = null

const main = async () => {
  await O.addStyle("style.css")
  
  const src = await O.rfs(info_file, 1)
  await init_info(src)
  
  div = O.ceDiv(O.body, "main")
  
  O.ael("popstate", evt => {
    set_state(evt.state)
  })
  
  let pth = O.urlParam("pth", "/v1")
  set_pth_str(pth)
}

const init_info = async src => {
  const [exprs_raw, cmds_raw] = JSON.parse(src)
  
  for(let i = 0; i !== exprs_raw.length; i++){
    const expr_raw = exprs_raw[i]
    const e = O.obj()
    
    for(let j = 0; j !== attribs_num; j++)
      e[attribs[j]] = expr_raw[j]
    
    e.id = i
    e.name0 = e.name
    e.ctor = e.ctor !== -1 ? e.ctor : null
    e.param = e.param !== -1 ? e.param : null
    
    if(e.name === null)
      e.name = get_name(e)
    
    mp_name_e.set(e.name, e)
    exprs.push(e)
  }
  
  for(const e of exprs){
    if(e.pair){
      const {pair} = e
      
      e.left = pair[0] = exprs[pair[0]]
      e.right = pair[1] = exprs[pair[1]]
    }
    
    e.cert = e.cert !== -1 ? exprs[e.cert] : null
  }
  
  for(const cmd of cmds_raw)
    cmds.push(cmd)
}

const render_page = () => {
  const {page_type, id} = state
  
  div.innerText = ""
  
  if(page_type === null){
    mk_links(div, [
      ["Expressions", "exprs"],
      ["Commands", "cmds"],
    ])
    return
  }
  
  if(page_type === "exprs")
    return render_page_exprs(id)
  
  e404()
}

const render_page_exprs = (id = null) => {
  if(id === null){
    let inp_search = O.ceInput(div, "input", "search")
    let results = O.ceDiv(div, "search-results")
    
    const search = s => {
      let es = []
      
      s = s.toLowerCase()
      
      for(let [name, e] of mp_name_e){
        if(name.toLowerCase().includes(s))
          es.push(e)
      }
      
      return es
    }
    
    const update = () => {
      let s = inp_search.value
      let es = search(s)
      
      results.innerText = ""
      
      for(const e of es.slice(0, search_max_res_num)){
        mk_link(results, e.name, "exprs", e.id)
      }
      
      let dif = es.length - search_max_res_num
      if(dif > 0) mk_text(results, `... and ${dif} more`)
    }
    
    O.ael(inp_search, "input", update)
    update()
    
    return
  }
  
  if(id < 0 || id >= exprs.length)
    return e404()
  
  let e = exprs[id]
  
  let info_div = O.ceDiv(div, "info-div")
  let info_left = O.ceDiv(info_div, "info-inner info-left")
  let info_right = O.ceDiv(info_div, "info-inner info-right")
  
  mk_text(info_left, "Name")
  mk_text(info_right, e.name || "/")
  
  mk_text(info_left, "ID")
  mk_text(info_right, show_nat(e.id))
  
  mk_text(info_left, "Pair")
  if(e.pair){
    let p = mk_block(info_right)
    
    if(display_pair_parens_in_info)
      mk_text(p, "(")
    
    mk_expr_link(p, e.left)
    mk_text(p, ", ")
    mk_expr_link(p, e.right)
    
    if(display_pair_parens_in_info)
      mk_text(p, ")")
  }else{
    mk_text(info_right, "/")
  }
  
  mk_text(info_left, "Certified")
  if(e.cert !== null){
    let p = mk_block(info_right)
    mk_green_text(p, "Yes")
    
    if(e.cert !== e){
      mk_text(p, " (from ")
      mk_expr_link(p, e.cert)
      mk_text(p, ")")
    }
  }else{
    mk_text(info_right, "/")
  }
  
  mk_text(info_left, "Constructor")
  mk_text(info_right, e.ctor !== null ? show_nat(e.ctor) : "/")
  
  mk_text(info_left, "Parameter")
  mk_text(info_right, e.param !== null ? show_nat(e.param) : "/")
  
  mk_text(info_left, "Parametrized")
  if(e.parametrized) mk_green_text(info_right, "Yes")
  else mk_text(info_right, "/")

  // cmd
}

const mk_div_block = div => {
  if(div.closest(".block"))
    return div
  
  return O.ceDiv(div, "block")
}

const mk_block = div => {
  let e = O.ceDiv(div, "block")
  return e
}

const mk_text = (div, text) => {
  div = mk_div_block(div)
  
  let e = O.ceDiv(div, "text")
  e.innerText = text
  
  return e
}

const mk_green_text = (div, text) => {
  let e = mk_text(div, text)
  e.classList.add("green")
  return e
}

const mk_link = (div, lab, page_type = null, id = null) => {
  if(state !== null){
    if(page_type === state.page_type && id === state.id)
      return mk_text(div, lab)
  }
  
  div = mk_div_block(div)
  
  let a = O.ce(div, "a")
  
  a.classList.add("link")
  a.innerText = lab
  a.href = calc_url(page_type, id)
  
  O.ael(a, "click", evt => {
    evt.preventDefault()
    set_pth(page_type, id)
  })
  
  return a
}

const mk_links = (e, xs) => {
  return xs.map(a => mk_link(e, ...a))
}

const mk_expr_link = (div, e) => {
  return mk_link(div, e.name, "exprs", e.id)
}

const calc_url = (page_type = null, id = null) => {
  let xs = ["", version_str]
  
  if(page_type !== null){
    xs.push(page_type)
    
    if(id !== null)
      xs.push(id)
  }
  
  let pth = xs.join("/")
  let url = location.href
    .replace(/&pth=[^&]*|$/, `&pth=${pth}`)
  
  return url
}

const set_pth = (page_type = null, id = null, push_state = 1) => {
  let state = {page_type, id}
  let url = calc_url(page_type, id)
  
  if(push_state)
    history.pushState(state, "", url)
  
  set_state(state)
}

const set_pth_str_aux = str => {
  let xs = str.split("/")
  
  if(xs.shift() !== "") return 0
  if(xs.shift() !== version_str) return 0
  
  let page_type = null
  let id = null
  
  if(xs.length !== 0){
    page_type = xs.shift()
    if(!page_types.includes(page_type)) return 0
  }
  
  if(xs.length !== 0){
    let index_raw = xs.shift()
    id = index_raw | 0
    if(index_raw !== String(id)) return 0
  }

  if(xs.length !== 0) return 0
  
  set_pth(page_type, id)
  return 1
}

const set_pth_str = str => {
  if(set_pth_str_aux(str)) return
  e404()
}

const e404 = () => {
  div.innerText = ""
  
  let h1 = O.ce(div, "h1")
  h1.innerText = "Error 404: Not found"
  
  mk_link(div, "Home")
}

const set_state = state1 => {
  if(state === state1) return
  
  state = state1
  render_page()
}

const get_name = e => {
  if(e.name !== null) return e.name
  if(e.ctor !== null) return `@${e.ctor}`
  if(e.param !== null) return `?${e.param}`
  
  return `\x5F${e.id}`
}

const show_nat = n => {
  let s = O.rev(n.toString())
  s = s.match(/\d{3}|\d+/g).join(",")
  return O.rev(s)
}

main().catch(log)