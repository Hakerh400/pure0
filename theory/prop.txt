nil0 = ();

#ctors {
  param_zero
  param_succ
  reduces
};

-----

#ctors {
  ident
  Z
  S
  imp
  wff
  proof
  ident_s
  wff_ident
  wff_imp
  ax1
  ax2
  mp
};

A = ident Z;
r1 = reduces (ident_s # ident ?0) #
  ident # S ?0;
r2 = reduces (wff_ident # ident ?0) #
  wff # ident ?0;
r3 = reduces (wff_imp (wff ?0) (wff ?1)) #
  wff # imp ?0 ?1;
r4 = reduces (ax1 (wff ?0) (wff ?1)) #
  proof # imp ?0 # imp ?1 ?0;
r5 = reduces (ax2 (wff ?0) (wff ?1) (wff ?2)) #
  proof # imp (imp ?0 # imp ?1 ?2) #
    imp (imp ?0 ?1) # imp ?0 ?2;
r6 = reduces (mp (proof # imp ?0 ?1) (proof ?0)) #
  proof ?1;

B = ident_s A;
C = ident_s B;

_A = wff_ident A;
_B = wff_ident B;
_C = wff_ident C;

_A2 = wff_imp _A _A;
x1 = ax1 _A _A;
x2 = ax1 _A _A2;
x3 = ax2 _A _A2 _A;
x4 = mp x3 x2;
x5 = mp x4 x1;

x5 == proof # imp A A;