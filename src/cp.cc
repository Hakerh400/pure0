// #include "log.hh"
// #include "cp.hh"
// 
// #ifdef FLAG_LINUX
//   const char* MODE_READ = "r";
//   const char* MODE_WRITE = "w";
// #endif
// #ifdef FLAG_WINDOWS
//   const char* MODE_READ = "rb";
//   const char* MODE_WRITE = "wb";
// #endif
// 
// namespace cp {
//   #ifdef COMPILER_DIR
//     const Prop has_compiler_dir = 1;
//     const String compiler_dir = COMPILER_DIR;
//   #else
//     const Prop has_compiler_dir = 0;
//     const String compiler_dir = "";
//   #endif
// 
//   String escape_arg(const String& arg){
//     for(char c : arg)
//       assert(!(c == '"' || c == '\\'));
// 
//     return "\"" + arg + "\"";
//   }
// 
//   String exec(
//     const String& exe,
//     const std::vector<String>& args
//   ){
//     String tmp_file1 = "/tmp/1.txt";
//     String tmp_file2 = "/tmp/2.txt";
// 
//     {
//       String cmd = "echo 0 > " + escape_arg(tmp_file1);
//       FILE* proc = popen(cmd.c_str(), MODE_WRITE);
// 
//       while(1){
//         String s = read_file(tmp_file1);
//         if(s.size() == 0) continue;
//         if(s.at(0) == '0') break;
//       }
// 
//       pclose(proc);
//     }
// 
//     String cmd = exe;
// 
//     for(const String& arg : args)
//       cmd += " " + escape_arg(arg);
// 
//     cmd += " > " + escape_arg(tmp_file2) +
//       " 2>&1 | echo 1 > " + escape_arg(tmp_file1);
//     FILE* proc = popen(cmd.c_str(), MODE_WRITE);
// 
//     while(1){
//       String s = read_file(tmp_file1);
//       if(s.size() == 0) continue;
//       if(s.at(0) == '0') break;
//     }
// 
//     pclose(proc);
// 
//     return read_file(tmp_file2);
//   }
// 
//   String run_node(const String& src){
//     String out0 = exec("node", {
//       "-e", "console.log((()=>{" + src + "})())",
//     });
// 
//     int len = out0.size();
//     assert(len != 0);
// 
//     int len1 = len - 1;
//     assert(out0[len1] == '\n');
// 
//     String out;
// 
//     for(int i = 0; i != len1; i++)
//       out += out0[i];
// 
//     return out;
//   }
// 
//   String gen_rand_buf_hex(int len){
//     int len2 = len << 1;
// 
//     String out = run_node(
//       "return crypto.randomBytes(" +
//       std::to_string(len) + ").toString('hex')"
//     );
// 
//     assert(out.size() == len2);
// 
//     return out;
//   }
// 
//   std::vector<uint8_t> gen_rand_buf(int len){
//     int len2 = len << 1;
// 
//     String s = gen_rand_buf_hex(len);
//     std::vector<uint8_t> buf;
// 
//     for(int i = 0; i != len2; i += 2){
//       int d1 = parse_hex_digit(s[i]);
//       int d2 = parse_hex_digit(s[i + 1]);
// 
//       buf.insert(buf.end(), (d1 << 4) | d2);
//     }
// 
//     return buf;
//   }
// 
//   String path_join(const String& pth1, const String& pth2){
//     return run_node(
//       "return path.join('" + pth1 + "','" +
//       pth2 + "')"
//     );
//   }
// 
//   String get_file_aux(const String& file_rel){
//     assert(has_compiler_dir);
//     return path_join(compiler_dir, file_rel);
//   }
// 
//   String get_dir_aux(const String& file_rel){
//     return path_join(get_file_aux(file_rel), "..");
//   }
// 
//   std::vector<String> get_files(const String& dir){
//     return sanl(run_node(
//       "let d='" + dir + "';return fs.readdirSync(d)" +
//       ".map(a=>path.join(d,a)).join(String.fromCharCode(10))"
//     ));
//   }
// 
//   Prop is_file(const String& pth){
//     return "1" == run_node(
//       "return fs.statSync('" + pth +
//       "').isFile() ? 1 : 0"
//     );
//   }
// 
//   Prop is_dir(const String& pth){
//     return "1" == run_node(
//       "return fs.statSync('" + pth +
//       "').isDirectory() ? 1 : 0"
//     );
//   }
// };