#pragma once

#include "util.hh"

namespace path {
  String join(const String&, const String&);
};