#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <cmath>
#include <vector>
#include <set>
#include <map>
#include <string>
#include <regex>
#include <unordered_set>
#include <unordered_map>
#include <functional>
#include <filesystem>

#define log _log_aux
#define floor _floor_aux
#define ceil _ceil_aux
#define sqrt _sqrt_aux
#define min _min_aux
#define max _max_aux
#define hypot _hypot_aux
#define dist _dist_aux
#define pow _pow_aux
#define sin _sin_aux
#define cos _cos_aux

#define HASH_INST(T, obj, f) \
  namespace std { \
    template<> \
    struct hash<T> { \
      size_t operator()(const T& obj) const f \
    }; \
  };

#define O \
  log(__LINE__);

namespace fs = std::filesystem;

typedef bool Prop;
typedef int64_t Int;
typedef uint64_t Nat;
typedef std::string String;

template <typename T>
using Set = std::unordered_set<T>;

template <typename K, typename V>
using Map = std::unordered_map<K, V>;

extern const int tab_size;
extern const String tab;

int floor(double);
int ceil(double);
double sqrt(double);
int bound(int, int, int);
double bound(double, double, double);
double min(double, double);
double max(double, double);
double hypot(double, double);
double dist(double, double, double, double);
double pow(double, double);
double sin(double);
double cos(double);
double log_2(double);

int read_int();
String read_str();
std::vector<int> read_int_vec();
String read_file(const String& pth);

int parse_hex_digit(char);
std::vector<String> sanl(const String&);
Prop ends_with(const String&, const String&);

Prop is_digit(char);
Prop is_lower_letter(char);
Prop is_upper_letter(char);
Prop is_letter(char);
Prop is_alphanum(char);
Prop is_whitespace(char);

int parse_nat(const String&);
char digit_to_hex(uint8_t);
String char_to_cc_hex(char);
