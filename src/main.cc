#include "main.hh"
#include "parser.hh"
#include "expr-info.hh"
#include "log.hh"
#include "rec.hh"

void main1(int argc, const char** argv){
  const String src_file = "./theory/main.txt";
  const String src = read_file(src_file);
  
  Parser* par = new Parser(src);
  Elab* elab = par->get_elab();
  
  fs::path proj_dir = fs::current_path();
  fs::path data_dir = proj_dir / "data";
  fs::path info_file = data_dir / "info.txt";
  
  if(!fs::exists(data_dir))
    fs::create_directory(data_dir);
  
  std::ofstream f {info_file};
  f << String {}
    + "[" + to_json(par->get_exprs_info())
    + "," + to_json(par->get_cmds())
  + "]";
  f.close();
  
  delete par;
}

int main(int argc, const char** argv){
  main1(argc, argv);
  return 0;
}