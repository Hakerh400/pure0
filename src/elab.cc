#include "elab.hh"
#include "log.hh"
#include "rec.hh"

const String ctor_param_zero = "_param_zero";
const String ctor_param_succ = "_param_succ";
const String ctor_reduces = "_reduces";
const String ctor_alt = "_alt";

Elab* Elab::elab = nullptr;

Prop z = 0;

void dbg_expr(int e){
  if(z) return;
  z = 1;
  assert(Elab::elab != nullptr);
  Elab::elab->disp(e);
  z = 0;
}

void dbg_expr(Expr e){
  dbg_expr(e);
}

Elab::Elab(){
  assert(Elab::elab == nullptr);
  Elab::elab = this;

  ctors_num = 0;
  nil0 = EXPR_NIL;
  nil = app(nil0, nil0);
  c_param_zero = ctor(ctor_param_zero, 0);
  c_param_succ = ctor(ctor_param_succ, 0);
  c_reduces = ctor(ctor_reduces, 0);
  c_alt = ctor(ctor_alt, 0);
  ctors_num = 0;

  param(0);
}

Elab::~Elab(){
  assert(Elab::elab == this);
  Elab::elab = nullptr;
}

int Elab::get_ctors_num() const {
  return ctors_num;
}

void Elab::set_ctors_num(int ctors_num){
  ctors_num = 0;
}

Expr Elab::get_nil(){
  return nil;
}

Expr Elab::mk_un(Expr e){
  return app(nil0, e);
}

Expr Elab::nat_to_ctor(int n){
  assert(n >= 0);

  if(mp_ctor_e.contains(n))
    return mp_ctor_e[n];

  Expr e = nil;

  for(int i = 0; i != n; i++)
    e = mk_un(e);

  mp_ctor_e[n] = e;
  mp_e_ctor[e] = n;

  return e;
}

Expr Elab::ctor(const String& name, Prop add_to_mp){
  if(mp_name_e.contains(name))
    return mp_name_e[name];

  int n = ctors_num++;
  Expr e = nat_to_ctor(n);

  if(add_to_mp){
    mp_name_e[name] = e;
    mp_e_name[e] = name;
  }

  return e;
}

void Elab::init_ctor(const String& name){
  ctor(name);
}

Expr Elab::param(const String& name){
  int len = name.length();
  assert(len == 1 || len == 2);
  if(len == 2) assert(name.at(0) == '?');
  return param(name.at(len - 1) - 'a');
}

Expr Elab::param(int n){
  assert(n >= 0);

  if(mp_param_e.contains(n))
    return mp_param_e[n];

  Expr e = c_param_zero;

  for(int i = 0; i <= n; i++)
    e = param_succ(e);

  mp_param_e[n] = e;
  mp_e_param[e] = n;

  return e;
}

Expr Elab::param_succ(Expr a){
  return app(c_param_succ, a);
}

Expr Elab::app(Expr a, Expr b){
  Rec _rec;
  
  Expr e = core::mk_pair(a, b);

  if(a == c_param_succ && mp_e_param.contains(b)){
    int n = mp_e_param[b] + 1;
    mp_e_param[e] = n;
    mp_param_e[n] = e;
  }

  // logb();
  Expr r = core::certify(e);
  assert(r != -1);
  // assert(st->is_inferred(r));
  // assert(r != nil0);
  
  if(core::get_left(a) == c_reduces){
    Expr lhs = core::get_right(a);
    
    if(!core::is_parametrized(lhs))
      core::revoke(lhs);
  }

  return r;
}

String Elab::show(int i, Prop raw){
  return show(i, raw);
}

String Elab::show(Expr e, Prop raw, int& pri1, int& pri2){
  Rec _rec;
  
  pri1 = 2;
  pri2 = 2;

  if(has_set_nat){
    if(!mp_e_nat.contains(e)){
      Expr e1 = e;
      int n = 0;

      while(1){
        if(e1 == nil0) break;

        if(mp_e_nat.contains(e1)){
          nat(n + mp_e_nat[e1]);
          break;
        }

        if(core::get_left(e1) != c_nat_succ) break;

        e1 = core::get_right(e1);
        n++;
      }
    }

    if(mp_e_nat.contains(e))
      return std::to_string(mp_e_nat[e]);
  }

  if(!raw && mp_e_name.contains(e)){
    String name = mp_e_name[e];

    if(name == "SetEmp") return "{}";

    return name;
  }

  if(e == nil0) return ".";

  if(mp_e_param.contains(e)){
    int i = mp_e_param[e];
    return String("?") + std::to_string(i);
  }

  Expr a = core::get_left(e);
  Expr b = core::get_right(e);

  int a1, a2;
  int b1, b2;

  String sa = show(a, 0, a1, a2);
  String sb = show(b, 0, b1, b2);

  String out {};

  do{
    if(a1 == 2 && a2 == 2){
      if(b1 == 2 && b2 == 2){
        pri1 = 1;
        pri2 = 2;
        out = sa + " " + sb;
        break;
      }

      if(b1 == 1 && b2 == 2){
        pri1 = 1;
        pri2 = 0;
        out = sa + " # " + sb;
        break;
      }

      if(b1 == 1 && b2 == 0){
        pri1 = 1;
        pri2 = 0;
        out = sa + " # " + sb;
        break;
      }
    }

    if(a1 == 1 && a2 == 2){
      if(b1 == 2 && b2 == 2){
        pri1 = 1;
        pri2 = 2;
        out = sa + " " + sb;
        break;
      }

      if(b1 == 1 && b2 == 2){
        pri1 = 1;
        pri2 = 0;
        out = sa + " # " + sb;
        break;
      }

      if(b1 == 1 && b2 == 0){
        pri1 = 1;
        pri2 = 0;
        out = sa + " # " + sb;
        break;
      }
    }

    if(a1 == 1 && a2 == 0){
      String s1 {};
      String s2 {};
      Prop p = 0;
      Prop p1 = 0;

      for(char c : sa){
        if(c == '#'){
          Prop p0 = p;
          p = 1;
          if(!p0) continue;
        }

        if(p){
          Prop p0 = p1;
          p1 = 1;
          if(!p0) continue;
        }

        (p == 0 ? s1 : s2) += c;
      }

      sa = s2;
      pri1 = 1;

      if(b1 == 1){
        pri2 = 0;
        sb = "# " + sb;
      }

      out = s1 + "(" + sa + ") " + sb;
      break;
    }

    log(std::vector<int>{a1, a2, b1, b2});
    assert(0);
  }while(0);

  do{
    String s0 = "Set # SetCons ";
    int n = s0.size();

    if(out.size() <= n) break;

    Prop p = 1;

    for(int i = 0; i != n; i++){
      if(out[i] != s0[i]){
        p = 0;
        break;
      }
    }

    if(!p) break;

    String s1 {};
    String s2 {};

    p = 0;

    for(int i = n; i != out.size(); i++){
      char c = out[i];

      if(p == 0){
        if(c == ' '){
          p = 1;
          i += 1;
          continue;
        }

        s1 += c;
        continue;
      }

      s2 += c;
    }

    pri1 = 2;
    pri2 = 2;
    out = "{" + s1 + (s2.at(0) == '}' ? "" : ", ") + s2;
  }while(0);

  return out;
}

String Elab::show(Expr e, Prop raw){
  int pri1, pri2;
  return show(e, raw, pri1, pri2);
}

String Elab::show(const String& name, Prop raw){
  return show(get(name), raw);
}

String Elab::show(const std::vector<Expr>& es, Prop raw){
  String s;

  int first = 1;

  for(Expr e : es){
    if(first) first = 0;
    else s += "\n";

    s += show(e, raw);
  }

  return s;
}

void Elab::disp(int i, Prop raw){
  disp(i, raw);
}

void Elab::disp(Expr e, Prop raw){
  log(show(e, raw));
}

void Elab::disp(const String& name, Prop raw){
  log(show(name, raw));
}

void Elab::disp(const std::vector<Expr>& es, Prop raw){
  log(show(es, raw));
}

Expr Elab::mk_reduction(Expr a, Expr b){
  return app(app(ctor(ctor_reduces), a), b);
}

Prop Elab::has(const String& name) const {
  return mp_name_e.contains(name);
}

Prop Elab::has(Expr e) const {
  return mp_e_name.contains(e);
}

Expr Elab::get(const String& name) const {
  if(!mp_name_e.contains(name)){
    log(name);
    assert(0);
  }

  return mp_name_e.at(name);
}

String Elab::get(Expr e) const {
  return mp_e_name.at(e);
}

void Elab::name(const String& name, Expr e){
  assert(!mp_name_e.contains(name));
  mp_name_e[name] = e;

  if(!mp_e_name.contains(e))
    mp_e_name[e] = name;
}

void Elab::set_nat(Expr zero, Expr succ){
  if(has_set_nat)
    clear_nat();

  has_set_nat = 1;
  c_nat_zero = zero;
  c_nat_succ = succ;
  mp_nat_e[0] = zero;
  mp_e_nat[zero] = 0;
}

void Elab::clear_nat(){
  has_set_nat = 0;
  mp_nat_e.clear();
  mp_e_nat.clear();
}

Expr Elab::nat(int n){
  assert(has_set_nat);
  assert(n >= 0);

  if(mp_nat_e.contains(n))
    return mp_nat_e[n];

  Expr e = c_nat_zero;

  for(int i = 0; i != n; i++){
    e = app(c_nat_succ, e);

    int k = i + 1;
    mp_nat_e[k] = e;
    mp_e_nat[e] = k;
  }

  return e;
}

void Elab::set_list(Expr nil, Expr cons){
  if(has_set_list)
    clear_list();

  has_set_list = 1;
  c_list_nil = nil;
  c_list_cons = cons;
}

void Elab::clear_list(){
  has_set_list = 0;
}

Expr Elab::list(const std::vector<Expr>& xs){
  assert(has_set_list);

  int len = xs.size();
  Expr e = c_list_nil;

  for(int i = len - 1; i >= 0; i--)
    e = app(app(c_list_cons, xs[i]), e);

  return e;
}

void Elab::set_set(Expr nil, Expr cons){
  if(has_set_set)
    clear_set();

  has_set_set = 1;
  c_set_nil = nil;
  c_set_cons = cons;
}

void Elab::clear_set(){
  has_set_set = 0;
}

Expr Elab::set(const std::vector<Expr>& xs){
  assert(has_set_set);

  int len = xs.size();
  Expr e = c_set_nil;

  for(int i = len - 1; i >= 0; i--)
    e = app(app(c_set_cons, xs[i]), e);

  return e;
}

Int Elab::get_exprs_num(){
  return core::get_construction_id();
}

Int Elab::get_rules_num(){
  return core::get_reduction_id();
}