#include "core.hh"
#include "log.hh"
#include "rec.hh"

ExprData::ExprData(Prop init){
  if(!init) return;
  
  construction_id = 1;
  exprs.insert(exprs.end(), {0, 0});
}

Prop core::reducing = 0;
ExprData core::data0 {1};
ExprData core::data1 {};
ExprData* core::data = &data0;

Int core::reduction_id = 0;

// Initially, the only certified expression is `()`
// and it is associated with `()`
Map<Expr, Expr> core::mp_cert_e {{0, 0}};

std::vector<Rule> core::rules {};
Map<Expr, Int> core::mp_e_rule {};

std::vector<Expr> core::ctors {};
Map<Expr, Int> core::mp_e_ctor {};

std::vector<Expr> core::params {};
Map<Expr, Int> core::mp_e_param {};

// Auxiliary constants and functions

Int core::CTOR_PARAM_ZERO = 0;
Int core::CTOR_PARAM_SUCC = 1;
Int core::CTOR_REDUCES = 2;
Int core::CTOR_ALT = 3;

Pair core::err_pair {-1, -1};

Prop core::is_pair(Expr e){
  return e != 0 && is_constructed(e);
}

const Pair& core::get_pair(Expr e){
  if(!is_pair(e)) return err_pair;
  
  if(e < data0.construction_id)
    return data0.exprs.at(e);
  
  return data1.exprs.at(e - data0.construction_id);
}

Prop core::is_primitive(Expr X){
  // [SPEC] Expression `X` is primitive iff
  // [SPEC]   `X` is `()` or
  // [SPEC]   `X` is a pair whose left or right element is `()`
  
  if(X == 0) return 1;
  
  auto [a, b] = get_pair(X);
  return a == 0 || b == 0;
}

Pair core::destruct_rule_expr(Expr e){
  auto [e1, rhs] = get_pair(e);
  auto [e2, lhs] = get_pair(e1);
  
  if(get_ctor(e2) == CTOR_REDUCES)
    return {lhs, rhs};
  
  return err_pair;
}

Expr core::mk_ctor(Int n){
  if(n < 0) return -1;
  
  Expr e = mk_pair(0, 0);
  while(n--) e = mk_pair(0, e);
  
  return e;
}

Prop core::match(Match& M, Expr P, Expr X){
  Rec _rec;
  
  // [SPEC] `() % ()` is the empty map
  if(P == 0 && X == 0) return 1;
  
  // [SPEC] if exactly one of `P` and `X` is nil
  // [SPEC]   the matching fails
  if(P == 0 || X == 0) return 0;
  
  // [SPEC] if `P` is not parametrized
  if(!is_parametrized(P)){
    // [SPEC] if `P` is `X`
    // [SPEC] then `P % X` is the empty map
    // [SPEC] otherwise the matching fails
    return P == X;
  }
  
  // [SPEC] For any natural number `n` and any expression `X`
  // [SPEC]   `(?n) % X` is the singleton map that maps `n` to `X`
  
  Int n = get_param(P);
  
  if(n != -1){
    if(M.contains(n))
      return M[n] == X;
    
    M[n] = X;
    return 1;
  }
  
  // [SPEC] For any pair `P` and any expressions `A` and `B`
  // [SPEC]   let `P` be `(P1, P2)`
  
  auto [P1, P2] = get_pair(P);
  auto [A, B] = get_pair(X);
  
  // [SPEC] let `S` be `P1 % A` and `T` be `P2 % B`
  // [SPEC] if `S` or `T` fail, then `P % (A, B)` fails
  if(!match(M, P1, A)) return 0;
  if(!match(M, P2, B)) return 0;
  
  // [SPEC] if there exists a natural number `n` such that
  // [SPEC]   `S` contains `n` and maps it to some expression `C` and
  // [SPEC]   `T` contains `n` and maps it to some expression `D` and
  // [SPEC]   `C` is not `D`
  // [SPEC] then the matching fails
  // [SPEC] otherwise `P % (A, B)` is the union of `S` and `T`
  
  // The above Specification is already handled when creating
  // a singleton map in the case when `P` is a param
  return 1;
}

Expr core::subst(const Match& M, Expr X){
  Rec _rec;
  
  // [SPEC] For any Match `M` and any non-parametrized expression `X`
  // [SPEC]   `M : X` is `X`
  if(!is_parametrized(X)) return X;
  
  // [SPEC] For any Match `M` and any natural number `n`
  Int n = get_param(X);
  if(n != -1){
    // [SPEC] if `M` contains `n` and maps it to some expression `Y`
    // [SPEC] then `M : (?n)` is `Y`
    // [SPEC] otherwise `M : (?n)` is `?n`
    if(M.contains(n)) return M.at(n);
    return X;
  }
  
  // [SPEC] For any Match `M` and any expressions `A` and `B`
  // [SPEC]   if for any natural number `n`, `(A, B)` is not `?n`
  // [SPEC]   then `M : (A, B)` is `(M : A, M : B)`
  
  auto [A, B] = get_pair(X);
  A = subst(M, A);
  B = subst(M, B);
  
  return mk_pair(A, B);
}

Expr core::reduce_aux(Map<Expr, Expr>& mp, Expr X){
  Rec _rec;
  
  Expr X0 = X;
  
  // The loop is used to avoid tail-call recursion
  while(1){
    if(mp.contains(X)) return mp[X];
    
    // [SPEC] if `X` is primitive, parametrized or certified
    // [SPEC]   return `X`
    if(is_primitive(X) || is_parametrized(X) || is_certified(X)){
      mp[X0] = X;
      return X;
    }
    
    // [SPEC] let `X` be `(A, B)`
    auto [A, B] = get_pair(X);
    
    // [SPEC] let `C` be the reduction of `A`
    Expr C = reduce_aux(mp, A);
    
    // [SPEC] let `D` be the reduction of `B`
    Expr D = reduce_aux(mp, B);
    
    // [SPEC] let `Y` be `(C, D)`
    Expr Y = mk_pair(C, D);
    
    // [SPEC] if `Y` is not `X`
    // [SPEC]   return the reduction of `Y`
    if(Y != X){
      // TCO
      X = Y;
      continue;
    }
    
    // [SPEC] let `xs` be the list of all reduction rules `L ~> R`
    // [SPEC]   such that `L % X` is a match
    // [SPEC] sort `xs` ascendingly by the rule id
    // [SPEC] let `ys` be `xs` with each element `L ~> R`
    // [SPEC]   replaced by the reduction of `(L % X) : R`
    // [SPEC] if `xs` is empty, return `X`
    // [SPEC] to process a list, do the following
    // [SPEC]   let `z` be the last element of the list
    // [SPEC]   let `zs` be the list without the last element
    // [SPEC]   if `zs` is empty, return `z`
    // [SPEC]   let `r` be the processed list `zs`
    // [SPEC]   return `((@3, r), z)`
    // [SPEC] return the reduction of the processed list `xs`
    
    // This represents the processed list
    Expr r = -1;
    
    for(const Rule& rule : rules){
      if(!rule.act) continue;
      
      Expr L = rule.lhs;
      Expr R = rule.rhs;
      
      Match M;
      if(!match(M, L, X)) continue;
      
      Expr z = subst(M, R);
      
      // If the list is empty
      if(r == -1){
        r = z;
        continue;
      }
      
      // Use the ALT ctor `@3` to combine
      // the old and the new expression
      
      Expr alt = mk_ctor(CTOR_ALT);
      r = mk_pair(mk_pair(alt, r), z);
    }
    
    // If the list is empty
    if(r == -1){
      mp[X0] = X;
      return X;
    }

    // TCO
    X = r;
  }
  
  assert(0);
  return -1;
}

Expr core::reduce(Expr X){
  assert(!reducing);
  
  if(!is_constructed(X)) return -1;
  
  reducing = 1;
  data1.construction_id = data0.construction_id;
  data = &data1;
  
  Map<Expr, Expr> mp;
  Expr Y = reduce_aux(mp, X);
  
  reducing = 0;
  data = &data0;
  
  if(Y != -1) Y = examine(Y);
  data1 = ExprData();
  
  return Y;
}

Expr core::examine_aux(Map<Expr, Expr>& mp, Int ci, Expr X){
  Rec _rec;
  
  // if `X` is constructed, then return
  if(is_constructed(X)) return X;
  if(mp.contains(X)) return mp[X];
  
  // let `X` be `(A, B)`
  auto [A, B] = data1.exprs.at(X - ci);
  
  A = examine_aux(mp, ci, A);
  B = examine_aux(mp, ci, B);
  
  // construct `X`
  Expr Y = mk_pair(A, B);
  mp[X] = Y;
  
  return Y;
}

Expr core::examine(Expr X){
  Map<Expr, Expr> mp {};
  Int ci = data0.construction_id;
  
  return examine_aux(mp, ci, X);
}

// === API ===

// --- Queries ---

Int core::get_construction_id(){
  return data->construction_id;
}

Int core::get_reduction_id(){
  return reduction_id;
}

Prop core::is_constructed(Expr e){
  return e >= 0 && e < data->construction_id;
}

Expr core::get_left(Expr e){
  return get_pair(e).first;
}

Expr core::get_right(Expr e){
  return get_pair(e).second;
}

Prop core::is_certified(Expr e){
  return mp_cert_e.contains(e);
}

Expr core::get_certified(Expr e){
  if(!is_certified(e)) return -1;
  return mp_cert_e.at(e);
}

Prop core::is_ctor(Expr e){
  return mp_e_ctor.contains(e);
}

Int core::get_ctor(Expr e){
  if(mp_e_ctor.contains(e))
    return mp_e_ctor[e];
  
  return -1;
}

Prop core::is_param(Expr e){
  return mp_e_param.contains(e);
}

Int core::get_param(Expr e){
  if(mp_e_param.contains(e))
    return mp_e_param[e];
  
  return -1;
}

Prop core::is_parametrized(Expr e){
  if(data0.parametrized.contains(e))
    return 1;
  
  if(reducing && data1.parametrized.contains(e))
    return 1;
  
  return 0;
}

// --- Actions ---

Expr core::mk_pair(Expr X, Expr Y){
  // [SPEC] given constructed expressions `X` and `Y`
  if(!is_constructed(X)) return -1;
  if(!is_constructed(Y)) return -1;
  
  // [SPEC] let `P` be `(X, Y)`
  Pair P {X, Y};
  
  // [SPEC] if `P` is not constructed
  // [SPEC]   insert `P` into constructed expressions
  // [SPEC]   associate `P` with the global construction id
  // [SPEC]   increment the global construction id
  // [SPEC] return `P`
  
  // The Specification treats a pair of expressions as an expression
  // However, in this implementation expression is identical to its id
  // Therefore, the id `e` is returned instead of pair `P`
  
  if(data0.mp_p_e.contains(P))
    return data0.mp_p_e[P];
  
  if(reducing && data1.mp_p_e.contains(P))
    return data1.mp_p_e[P];
  
  Expr e = data->construction_id++;
  data->exprs.insert(data->exprs.end(), P);
  data->mp_p_e[P] = e;
  
  // Check for ctor
  while(X == 0){
    Int ctor = ctors.size();
    
    if(Y == 0){
      assert(e == 1);
      assert(ctor == 0);
    }else{
      Int ctor0 = get_ctor(Y);
      if(ctor0 == -1) break;
      
      assert(ctor == ctor0 + 1);
    }
    
    ctors.insert(ctors.end(), e);
    mp_e_ctor[e] = ctor;
    
    break;
  }
  
  Prop is_param = 0;
  
  // Check for param
  while(get_ctor(X) == CTOR_PARAM_SUCC){
    Int param = params.size();
    
    if(get_ctor(Y) == CTOR_PARAM_ZERO){
      assert(param == 0);
    }else{
      Int param0 = get_param(Y);
      if(param0 == -1) break;
      
      assert(param == param0 + 1);
    }
    
    params.insert(params.end(), e);
    mp_e_param[e] = param;
    is_param = 1;
    
    break;
  }
  
  // Check if the expression is parametrized
  if(is_param || is_parametrized(X) || is_parametrized(Y))
    data->parametrized.insert(e);
  
  return e;
}

Expr core::certify(Expr P){
  // [SPEC] let `P` be `(X, Y)`
  auto [X, Y] = get_pair(P);
  
  // [SPEC] assert that `X` and `Y` are certified
  if(!is_certified(X)) return -1;
  if(!is_certified(Y)) return -1;
  
  // [SPEC] let `Q` be the reduction of `P`
  // [SPEC] examine `Q`
  Expr Q = reduce(P);
  
  // [SPEC] if `Q` is not certified
  if(!is_certified(Q)){
    // [SPEC] insert `Q` into certified expressions
    // [SPEC] associate `Q` with `P`
    mp_cert_e[Q] = P;
    
    auto [lhs, rhs] = destruct_rule_expr(Q);
    
    // [SPEC] if `Q` is a reduction rule
    if(lhs != -1){
      // [SPEC] insert `Q` into reduction rules
      // [SPEC] associate `Q` with the global reduction rule id
      // [SPEC] increment the global reduction rule id
      
      Rule rule {Q, lhs, rhs, 1};
      
      rules.insert(rules.end(), rule);
      mp_e_rule[Q] = reduction_id++;
    }
  }
  
  // [SPEC] return `Q`
  return Q;
}

void core::revoke(Expr X){
  // [SPEC] given certified expression `X`
  if(!is_certified(X)) return;
  
  // [SPEC] remove `X` from certified expressions
  mp_cert_e.erase(X);
  
  // [SPEC] if `X` is a reduction rule
  if(mp_e_rule.contains(X)){
    // [SPEC] remove `X` from reduction rules
    
    Int rule_id = mp_e_rule[X];
    Rule& rule = rules.at(rule_id);
    
    assert(rule.act);
    rule.act = 0;
    
    mp_e_rule.erase(X);
  }
}