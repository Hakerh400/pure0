#include "rec.hh"
#include "log.hh"

int Rec::mx = 1000;
int Rec::n = 0;

Rec::Rec(){
  if(++n > mx){
    logb();
    log("Stack overflow");
    exit(1);
  }
}

Rec::~Rec(){
  n--;
}