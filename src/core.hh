#pragma once

#include "util.hh"

typedef Int Expr;
typedef std::pair<Expr, Expr> Pair;
typedef Map<Int, Expr> Match;

HASH_INST(Pair, p, {
  Expr a = p.first;
  Expr b = p.second;
  return a ^ (b + 0x9e3779b9 + (a << 6) + (a >> 2));
})

struct Rule {
  Rule(Expr expr, Expr lhs, Expr rhs, Prop act) :
    expr(expr), lhs(lhs), rhs(rhs), act(act){}
  
  // The expression that represents the reduction rule
  Expr expr;
  
  // LHS and RHS
  Expr lhs, rhs;
  
  // True iff the expression is not revoked
  Prop act;
};

struct ExprData {
  ExprData(Prop init=0);
  
  Int construction_id = 0;

  // Map from expressions to pairs
  // Implemented as a vector for better performance
  std::vector<Pair> exprs {};
  
  // Map from pairs of expressions to their ids
  Map<Pair, Expr> mp_p_e {};
  
  // Set of parametrized expressions
  Set<Expr> parametrized {};
};

class core {
private:
  /*
    In order to implement transactions (begin, commit, rollback),
    it is necessary to save the old state and provide an interface
    for discarding the old state and for restoring the old state.
    This is primarily implemented for reductions. Structures that
    can change during reduction are construction id, the collection
    of constructed expressions (`expr` and `mp_p_e`) and the set of
    parametrized expressions. Other structures, such as `ctors` and
    `params`, may change, but the changes do not affect the reduction
    process. They will be updated in the process of examination.
    
    Structure `data0` is contains permanent data, while `data1`
    contains the delta between the beginning of the reduction
    and the current time. After the reduction, final expression
    will be examined using data from `data1` and will be permanently
    stored in `data0`. The `data1` will be erased afterwards.
  */
  
  static Prop reducing;
  static ExprData data0;
  static ExprData data1;
  static ExprData* data;
  
  static Int reduction_id;
  
  // Map from certified reduced expressions
  // to their original form
  static Map<Expr, Expr> mp_cert_e;
  
  // Map from reduction rule ids to reduction rules
  // Implemented as a vector for better performance
  static std::vector<Rule> rules;
  
  // Map from expressions to reduction rule ids
  static Map<Expr, Int> mp_e_rule;
  
  // Map from ctors to expressions
  // Implemented as a vector for better performance
  static std::vector<Expr> ctors;
  
  // Map from expressions to ctors
  static Map<Expr, Int> mp_e_ctor;
  
  // Map from params to expressions
  // Implemented as a vector for better performance
  static std::vector<Expr> params;
  
  // Map from expressions to params
  static Map<Expr, Int> mp_e_param;
  
  // Auxiliary constants and functions
  
  static Int CTOR_PARAM_ZERO;
  static Int CTOR_PARAM_SUCC;
  static Int CTOR_REDUCES;
  static Int CTOR_ALT;
  
  static Pair err_pair;
  
  static Prop is_pair(Expr);
  static const Pair& get_pair(Expr);
  static Prop is_primitive(Expr);
  static Pair destruct_rule_expr(Expr);
  static Expr mk_ctor(Int);
  
  static Prop match(Match&, Expr, Expr);
  static Expr subst(const Match&, Expr);
  static Expr reduce_aux(Map<Expr, Expr>&, Expr);
  static Expr reduce(Expr);
  static Expr examine_aux(Map<Expr, Expr>&, Int, Expr);
  static Expr examine(Expr);

public: // === API ===
  // --- Queries ---
  
  static Int get_construction_id();
  static Int get_reduction_id();
  static Prop is_constructed(Expr);
  static Expr get_left(Expr);
  static Expr get_right(Expr);
  static Prop is_certified(Expr);
  static Expr get_certified(Expr);
  static Prop is_ctor(Expr);
  static Int get_ctor(Expr);
  static Prop is_param(Expr);
  static Int get_param(Expr);
  static Prop is_parametrized(Expr);
  
  // --- Actions ---
  
  static Expr mk_pair(Expr, Expr);
  static Expr certify(Expr);
  static void revoke(Expr);
};