#include "expr-info.hh"
#include "parser.hh"
#include "log.hh"

/*
  Proof info includes
    name :: String
    pair :: (E, E)
    cert :: Maybe E
    ctor :: Maybe N
    param :: Maybe N
    parametrized :: Prop
    cmd :: N
*/

String ExprInfo::to_json() const {
  const Elab* elab = par->get_elab();

  String s_name = !elab->has(e) ? "null" :
    ::to_json(elab->get(e));

  String s_pair = e == EXPR_NIL ? "null" :
    String {"["} + std::to_string(core::get_left(e)) +
    "," + std::to_string(core::get_right(e)) + "]";

  String s_cert = std::to_string(core::get_certified(e));
  String s_ctor = std::to_string(core::get_ctor(e));
  String s_param = std::to_string(core::get_param(e));
  String s_parametrized = core::is_parametrized(e) ? "1" : "0";

  int cmd_i = par->get_expr_cmd(e);
  String s_cmd = cmd_i == -1 ? "null" :
    std::to_string(cmd_i);

  return String {}
    /* 0 */ + "[" + s_name
    /* 1 */ + "," + s_pair
    /* 2 */ + "," + s_cert
    /* 3 */ + "," + s_ctor
    /* 4 */ + "," + s_param
    /* 5 */ + "," + s_parametrized
    /* 6 */ + "," + s_cmd
    + "]";
}