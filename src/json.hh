#pragma once

#include "util.hh"

class ToJson {
public:
  virtual String to_json() const;
};

template <typename T>
std::enable_if_t<!std::is_base_of_v<ToJson, T>, String>
to_json(const T&);

template <typename T>
std::enable_if_t<std::is_base_of_v<ToJson, T>, String>
to_json(const T& x){
  return x.to_json();
}

template <typename T>
String to_json(const std::vector<T>& xs){
  String s {};
  s += '[';
  
  Prop first = 0;
  
  for(const T& x : xs){
    if(!first) first = 1;
    else s += ',';
    
    s += to_json(x);
  }
  
  s += ']';
  
  return s;
}

template <typename T>
String to_json(const Map<String, T>& mp){
  String s {};
  s += '{';
  
  Prop first = 0;
  
  for(const std::pair<String, T>& p : mp){
    if(!first) first = 1;
    else s += ',';
    
    const String& key = p.first;
    const T& val = p.second;
    
    s += to_json(key) + ":" + to_json(val);
  }
  
  s += '}';
  
  return s;
}