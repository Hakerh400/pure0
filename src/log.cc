#include "log.hh"

template<typename T>
void log_aux(T x, Prop endl){
  std::cout << x;
  if(endl) std::cout << std::endl;
  fflush(stdout);
}

void log(){
  log_aux("");
}

void log(char c, Prop endl){
  log_aux(c, endl);
}

void log(const char* s, Prop endl){
  log_aux(s, endl);
}

void log(int32_t n, Prop endl){
  log_aux(n, endl);
}

void log(uint32_t n, Prop endl){
  log_aux(n, endl);
}

void log(int64_t n, Prop endl){
  log_aux(n, endl);
}

void log(uint64_t n, Prop endl){
  log_aux(n, endl);
}

void log(double x, Prop endl){
  log_aux(x, endl);
}

void log(const void* ptr, Prop endl){
  log_aux(ptr, endl);
}

void log(const String& s, Prop endl){
  log_aux(s, endl);
}

// template<typename T>
// void log(const std::vector<T>&, Prop endl){
//   log("[", 0);
// 
//   int n = xs.size();
// 
//   for(int i = 0; i != n; i++){
//     if(i != 0) log(", ", 0);
//     log(xs.at(i));
//   }
// 
//   log("]", endl);
// }

void logb(){
  log("");
  
  for(int i = 0; i != 100; i++)
    log("=", 0);
  
  log("\n");
}