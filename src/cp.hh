// #pragma once
// 
// #include "path.hh"
// 
// namespace cp {
//   extern const Prop has_compiler_dir;
//   extern const String compiler_dir;
// 
//   String escape_arg(const String&);
// 
//   String exec(
//     const String&,
//     const std::vector<String>& = {}
//   );
// 
//   String run_node(const String&);
// 
//   String gen_rand_buf_hex(int);
//   std::vector<uint8_t> gen_rand_buf(int);
//   String path_join(const String&, const String&);
// 
//   String get_file_aux(const String&);
//   String get_dir_aux(const String&);
// 
//   #define get_file() get_file_aux(__FILE__)
//   #define get_dir() get_dir_aux(__FILE__)
// 
//   std::vector<String> get_files(const String&);
//   Prop is_file(const String&);
//   Prop is_dir(const String&);
// };