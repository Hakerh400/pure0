#include "parser.hh"
#include "expr-info.hh"
#include "log.hh"
#include "rec.hh"

#define DBG(a) { \
  logb(); \
  log(__LINE__); \
  log(cmd); \
  log(a); \
  exit(1); \
}

#define DBG_TOKS(toks) DBG(show_toks(toks))

#define ASRT_FRESH(name) { \
  if(elab->has(name)){ \
    log("Redefinition of \"" + name + "\""); \
    exit(1); \
  } \
}

String Parser::show_toks(const std::vector<Token>& toks){
  int n = toks.size();
  String s {};

  for(int i = 0; i != n; i++){
    if(i != 0) s += " ";
    s += toks[i].str;
  }

  return s;
}

Parser::Parser(const String& src){
  elab = new Elab();
  this->src = src;
  tokenize();
}

Parser::~Parser(){
  delete elab;
}

Elab* Parser::get_elab() const {
  return elab;
}

int Parser::get_src_index() const {
  return src_index;
}

int Parser::get_tok_index() const {
  return tok_index;
}

Prop Parser::src_eof() const {
  return src_index == src.size();
}

Prop Parser::toks_eof() const {
  return tok_index == toks.size();
}

Expr Parser::get(const String& name){
  if(!elab->has(name)){
    log("Undefined identifier \"" + name + "\"");
    exit(1);
  }

  return elab->get(name);
}

void Parser::tokenize(){
  Token tok {};

  std::function<void(void)> push_tok = [&](){
    if(tok.type != TokenType::Comment)
      toks.insert(toks.end(), tok);

    tok.reset();
  };

  std::function<void(char)> push_char = [&](char c){
    if(tok.null()){
      if(is_whitespace(c)) return;

      if(is_fst_ident_char(c)){
        tok.type = TokenType::Ident;
        tok.push(c);
        return;
      }

      if(is_digit(c)){
        tok.type = TokenType::_Nat;
        tok.push(c);
        return;
      }

      if(is_op_char(c)){
        tok.type = TokenType::Op;
        tok.push(c);
        return;
      }

      if(is_open_paren_char(c)){
        tok.type = TokenType::OpenParen;
        tok.push(c);
        push_tok();
        return;
      }

      if(is_closed_paren_char(c)){
        tok.type = TokenType::ClosedParen;
        tok.push(c);
        push_tok();
        return;
      }

      if(is_open_bracket_char(c)){
        tok.type = TokenType::OpenBracket;
        tok.push(c);
        push_tok();
        return;
      }

      if(is_closed_bracket_char(c)){
        tok.type = TokenType::ClosedBracket;
        tok.push(c);
        push_tok();
        return;
      }

      if(is_open_brace_char(c)){
        tok.type = TokenType::OpenBrace;
        tok.push(c);
        push_tok();
        return;
      }

      if(is_closed_brace_char(c)){
        tok.type = TokenType::ClosedBrace;
        tok.push(c);
        push_tok();
        return;
      }

      if(c == ';'){
        tok.type = TokenType::Semicolon;
        tok.push(c);
        push_tok();
        run_cmd();
        return;
      }

      if(c == '?'){
        tok.type = TokenType::Parameter;
        // tok.push('0');
        return;
      }

      if(c == ','){
        tok.type = TokenType::Comma;
        tok.push(',');
        push_tok();
        return;
      }

      DBG(c);
      return;
    }

    TokenType t = tok.type;

    if(t == TokenType::Ident){
      if(is_ident_char(c)){
        tok.push(c);
        return;
      }

      push_tok();
      push_char(c);
      return;
    }

    if(t == TokenType::_Nat){
      if(is_digit(c)){
        tok.push(c);
        return;
      }

      push_tok();
      push_char(c);
      return;
    }

    if(t == TokenType::Parameter){
      if(is_digit(c)){
        tok.push(c);
        return;
      }

      push_tok();
      push_char(c);
      return;
    }

    if(t == TokenType::Op){
      if(is_op_char(c)){
        if(tok == "-" && c == '-'){
          tok.type = TokenType::Comment;
          return;
        }

        tok.push(c);
        return;
      }

      push_tok();
      push_char(c);
      return;
    }

    if(t == TokenType::Comment){
      if(c == '\r' || c == '\n')
        push_tok();

      return;
    }

    DBG(std::make_pair(t, c));
  };

  for(char c : src){
    if(!(cmd.size() == 0 && is_whitespace(c)))
      cmd += c;

    push_char(c);
  }

  if(!tok.null())
    push_tok();

  if(toks.size() != 0) DBG(toks);
}

const Token& Parser::get_tok(Prop advance){
  const Token& tok = toks.at(tok_index);
  if(advance) tok_index++;
  return tok;
}

void Parser::get_tok(const String& str, Prop advance){
  assert(get_tok(advance) == str);
}

String Parser::get_ident(Prop advance){
  const Token& tok = get_tok(advance);
  assert(tok.type == TokenType::Ident);
  return tok.str;
}

void Parser::get_ident(const String& str, Prop advance){
  assert(get_ident(advance) == str);
}

void Parser::run_cmd(){
  int cmd_i = cmds.size();

  cmds.insert(cmds.end(), cmd);

  do{
    const Token& tok = get_tok();

    if(tok.type == TokenType::Ident){
      if(get_tok(0) == "="){
        get_tok();

        const String& name = tok.str;

        Expr e = parse_expr();
        get_tok(";");

        ASRT_FRESH(name);
        elab->name(name, e);

        break;
      }

      if(tok == "show"){
        Expr e = parse_expr();
        get_tok(";");
        elab->disp(e);
        break;
      }

      if(tok == "show'"){
        Expr e = parse_expr();
        get_tok(";");
        elab->disp(e, 1);
        break;
      }

      if(tok == "nat"){
        if(get_tok(0) == ";"){
          get_tok();
          elab->clear_nat();
          break;
        }

        const Token& tok1 = get_tok();
        const Token& tok2 = get_tok();

        if(tok1.type != TokenType::Ident) DBG(tok1);
        if(tok2.type != TokenType::Ident) DBG(tok2);

        get_tok(";");
        elab->set_nat(get(tok1.str), get(tok2.str));

        break;
      }

      if(tok == "list"){
        if(get_tok(0) == ";"){
          get_tok();
          elab->clear_list();
          break;
        }

        const Token& tok1 = get_tok();
        const Token& tok2 = get_tok();

        if(tok1.type != TokenType::Ident) DBG(tok1);
        if(tok2.type != TokenType::Ident) DBG(tok2);

        get_tok(";");
        elab->set_list(get(tok1.str), get(tok2.str));

        break;
      }

      if(tok == "set"){
        if(get_tok(0) == ";"){
          get_tok();
          elab->clear_list();
          break;
        }

        const Token& tok1 = get_tok();
        const Token& tok2 = get_tok();

        if(tok1.type != TokenType::Ident) DBG(tok1);
        if(tok2.type != TokenType::Ident) DBG(tok2);

        get_tok(";");
        elab->set_set(get(tok1.str), get(tok2.str));

        break;
      }

      if(tok == "ctors"){
        get_tok("{");

        while(1){
          const Token& tok = get_tok(0);

          if(tok == "}"){
            get_tok();
            break;
          }

          if(tok.type == TokenType::Ident){
            get_tok();

            const String& name = tok.str;

            ASRT_FRESH(name);
            elab->ctor(name);

            continue;
          }

          DBG(tok);
        }

        get_tok(";");

        break;
      }

      if(tok == "enum"){
        String type_name = get_ident();

        ASRT_FRESH(type_name);
        Expr t = elab->ctor(type_name);

        get_tok("{");

        while(1){
          const Token& tok = get_tok(0);

          if(tok == "}"){
            get_tok();
            break;
          }

          if(tok.type == TokenType::Ident){
            get_tok();

            const String& name = tok.str;
            String ctor_name = type_name + name;

            ASRT_FRESH(name);
            ASRT_FRESH(ctor_name);

            Expr c = elab->ctor(ctor_name);
            elab->name(name, elab->app(t, c));

            continue;
          }

          DBG(tok);
        }

        get_tok(";");

        break;
      }

      if(tok == "exit"){
        exit(0);
        return;
      }

      if(tok == "eq"){
        Expr e1 = parse_expr1();
        Expr e2 = parse_expr1();

        get_tok(";");

        if(e1 != e2){
          log("Expression mismatch\n\n" +
            tab + elab->show(e1) + "\n" +
            tab + elab->show(e2));
          exit(1);
        }

        break;
      }

      if(tok == "def"){
        // log(show_toks(toks));

        String name = get_ident();

        Expr ctor = elab->has(name) ?
          elab->get(name) : elab->ctor(name);

        int args_num = 0;
        int cur_args_num = 0;
        Expr lhs = ctor;

        std::function<void(Expr)> push_case = [&](Expr rhs){
          if(args_num == 0){
            if(cur_args_num == 0){
              log("Case has no arguments in the definition of \"" +
                name + "\"");
              exit(1);
            }
            args_num = cur_args_num;
          }else if(cur_args_num != args_num){
            log("Too few arguments in the definition of \"" +
              name + "\"");
            exit(1);
          }

          elab->app(elab->app(elab->ctor("reduces"), lhs), rhs);

          cur_args_num = 0;
          lhs = ctor;
        };

        while(1){
          const Token& tok = get_tok(0);

          if(tok == "=" || tok == "->"){
            get_tok();
            push_case(parse_expr());

            const Token& tok = get_tok();

            if(tok == ",") continue;
            if(tok == ";") break;

            DBG(tok);
            return;
          }

          Expr arg = parse_expr1();

          if(args_num != 0 && cur_args_num == args_num){
            log("Too many arguments in the definition of \"" +
              name + "\"");
            exit(1);
          }

          cur_args_num++;
          lhs = elab->app(lhs, arg);
        }

        break;
      }

      DBG(tok);
      return;
    }

    DBG(tok);
    return;
  }while(0);

  if(!toks_eof()) DBG_TOKS(toks);

  toks.clear();
  tok_index = 0;
  cmd = "";

  int es_num = elab->get_exprs_num();

  for(int i = last_cmd_es_num; i != es_num; i++)
    mp_e_cmd[i] = cmd_i;

  last_cmd_es_num = es_num;
}

Expr Parser::parse_expr(Prop sngl){
  Rec _rec;
  
  Prop fst = 1;
  Expr e0;

  std::function<void(Expr)> push_expr = [&](Expr e){
    if(fst){
      fst = 0;
      e0 = e;
      return;
    }

    e0 = elab->app(e0, e);
  };

  while(!sngl || fst){
    const Token& tok = get_tok();

    if(tok.type == TokenType::Ident){
      push_expr(get(tok.str));
      continue;
    }

    if(tok.type == TokenType::_Nat){
      push_expr(elab->nat(parse_nat(tok.str)));
      continue;
    }

    if(tok.type == TokenType::Parameter){
      push_expr(elab->param(parse_nat(tok.str)));
      continue;
    }

    if(tok == "("){
      if(get_tok(0) == ")"){
        get_tok();
        push_expr(EXPR_NIL);
        continue;
      }

      push_expr(parse_expr());
      get_tok(")");
      continue;
    }

    if(tok == "["){
      std::vector<Expr> xs {};

      while(1){
        if(get_tok(0) == "]"){
          get_tok();
          break;
        }

        if(xs.size() != 0)
          get_tok(",");

        Expr e = parse_expr();
        xs.insert(xs.end(), e);
      }

      push_expr(elab->list(xs));
      continue;
    }

    if(tok == "{"){
      std::vector<Expr> xs {};

      while(1){
        if(get_tok(0) == "}"){
          get_tok();
          break;
        }

        if(xs.size() != 0)
          get_tok(",");

        Expr e = parse_expr();
        xs.insert(xs.end(), e);
      }

      push_expr(elab->set(xs));
      continue;
    }

    if(tok == "#"){
      push_expr(parse_expr());
      continue;
    }

    tok_index--;
    break;
  }

  if(fst) DBG(get_tok(0));

  return e0;
}

Expr Parser::parse_expr1(){
  return parse_expr(1);
}

const std::vector<String>& Parser::get_cmds() const {
  return cmds;
}

std::vector<ExprInfo> Parser::get_exprs_info() const {
  int n = elab->get_exprs_num();
  std::vector<ExprInfo> es {};

  for(int i = 0; i != n; i++)
    es.insert(es.end(), ExprInfo(this, i));

  return es;
}

int Parser::get_expr_cmd(int e) const {
  if(!mp_e_cmd.contains(e)) return -1;
  return mp_e_cmd.at(e);
}

#undef DBG