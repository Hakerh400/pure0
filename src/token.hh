#pragma once

#include "json.hh"

enum TokenType {
  None,
  Ident,
  _Nat,
  Op,
  OpenParen,
  ClosedParen,
  OpenBracket,
  ClosedBracket,
  OpenBrace,
  ClosedBrace,
  Semicolon,
  Comment,
  Parameter,
  Comma,
};

struct Token {
  TokenType type = TokenType::None;
  String str {};

  Prop null() const;
  void reset();
  void push(char);

  Prop operator==(const String&) const;
};

void log(const Token&, Prop endl = 1);

Prop is_fst_ident_char(char);
Prop is_ident_char(char);
Prop is_op_char(char);
Prop is_open_paren_char(char);
Prop is_closed_paren_char(char);
Prop is_open_bracket_char(char);
Prop is_closed_bracket_char(char);
Prop is_open_brace_char(char);
Prop is_closed_brace_char(char);