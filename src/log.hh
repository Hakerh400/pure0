#pragma once

#include "util.hh"

template<typename T>
void log_aux(T, Prop endl=1);

void log();
void log(char, Prop endl=1);
void log(const char*, Prop endl=1);
void log(int32_t, Prop endl=1);
void log(uint32_t, Prop endl=1);
void log(int64_t, Prop endl=1);
void log(uint64_t, Prop endl=1);
void log(double, Prop endl=1);
void log(const void*, Prop endl=1);
void log(const String&, Prop endl=1);
void logb();
template<typename A, typename B> void log(const std::pair<A, B>&, Prop endl=1);
template<typename T> void log(const std::vector<T>&, Prop endl=1);
template<typename T> void log(const Set<T>&, Prop endl=1);
template<typename K, typename V> void log(const Map<K, V>&, Prop endl=1);

template<typename A, typename B>
void log(const std::pair<A, B>& p, Prop endl){
  log("(", 0);
  log(p.first, 0);
  log(", ", 0);
  log(p.second, 0);
  log(")", endl);
}

template<typename T>
void log(const std::vector<T>& xs, Prop endl){
  log("[", 0);

  int i = 0;

  for(const T& x : xs){
    if(i++ != 0) log(", ", 0);
    log(x, 0);
  }

  log("]", endl);
}

template<typename T>
void log(const Set<T>& set, Prop endl){
  log("{", 0);

  int i = 0;

  for(const T& x : set){
    if(i++ != 0) log(", ", 0);
    log(x, 0);
  }

  log("}", endl);
}

template<typename K, typename V>
void log(const Map<K, V>& mp, Prop endl){
  log("{", 0);

  int i = 0;

  for(const std::pair<K, V>& p : mp){
    if(i++ != 0) log(", ", 0);
    log("(", 0);
    log(p.first, 0);
    log(" -> ", 0);
    log(p.second, 0);
    log(")", 0);
  }

  log("}", endl);
}