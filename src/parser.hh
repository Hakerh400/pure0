#pragma once

#include "elab.hh"
#include "token.hh"

class ExprInfo;

class Parser {
public:
  static String show_toks(const std::vector<Token>&);

  Parser(const String&);
  ~Parser();
  Elab* get_elab() const;
  int get_src_index() const;
  int get_tok_index() const;
  Prop src_eof() const;
  Prop toks_eof() const;
  const std::vector<String>& get_cmds() const;
  std::vector<ExprInfo> get_exprs_info() const;
  int get_expr_cmd(int) const;

private:
  Elab* elab;
  String src;
  std::vector<Token> toks;
  int src_index = 0;
  int tok_index = 0;
  int last_cmd_es_num = 0;
  Map<int, int> mp_e_cmd;

  String cmd;
  std::vector<String> cmds;

  Expr get(const String&);
  void tokenize();
  const Token& get_tok(Prop advance = 1);
  void get_tok(const String&, Prop advance = 1);
  String get_ident(Prop advance = 1);
  void get_ident(const String&, Prop advance = 1);
  void run_cmd();
  Expr parse_expr(Prop sngl = 0);
  Expr parse_expr1();
};