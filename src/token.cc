#include "log.hh"
#include "token.hh"

Prop Token::null() const {
  // Prop b1 = str.size() == 0;
  Prop b2 = type == TokenType::None;

  // assert(b1 == b2);

  // return b1;
  return b2;
}

void Token::reset(){
  type = TokenType::None;
  str = "";
}

void Token::push(char c){
  assert(type != TokenType::None);
  str += c;
}

Prop Token::operator==(const String& str) const {
  return this->str == str;
}

void log(const Token& tok, Prop endl){
  log(tok.str, endl);
}

Prop is_fst_ident_char(char c){
  return is_letter(c) || c == '\x5F';
}

Prop is_ident_char(char c){
  return is_fst_ident_char(c) || is_digit(c) || c == '\'';
}

Prop is_op_char(char c){
  return (0
    || c == '='
    || c == '#'
    || c == '-'
    || c == '<'
    || c == '>'
    || c == '@'
    || c == ':'
  );
}

Prop is_open_paren_char(char c){
  return (0
    || c == '('
  );
}

Prop is_closed_paren_char(char c){
  return (0
    || c == ')'
  );
}

Prop is_open_bracket_char(char c){
  return (0
    || c == '['
  );
}

Prop is_closed_bracket_char(char c){
  return (0
    || c == ']'
  );
}

Prop is_open_brace_char(char c){
  return (0
    || c == '{'
  );
}

Prop is_closed_brace_char(char c){
  return (0
    || c == '}'
  );
}