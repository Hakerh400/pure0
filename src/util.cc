#include "util.hh"

const int tab_size = 2;

String calc_tab_str(){
  String s;

  for(int i = 0; i != tab_size; i++)
    s += ' ';

  return s;
}

const String tab = calc_tab_str();

int floor(double x){
  #undef floor
  return static_cast<int>(std::floor(x));
  #define floor _floor_aux
}

int ceil(double x){
  #undef ceil
  return static_cast<int>(std::ceil(x));
  #define ceil _ceil_aux
}

double sqrt(double x){
  #undef sqrt
  return std::sqrt(x);
  #define sqrt _sqrt_aux
}

int bound(int val_min, int val_max, int val){
  if(val < val_min) return val_min;
  if(val > val_max) return val_max;
  return val;
}

double bound(double val_min, double val_max, double val){
  if(val < val_min) return val_min;
  if(val > val_max) return val_max;
  return val;
}

double min(double a, double b){
  if(a < b) return a;
  return b;
}

double max(double a, double b){
  if(a > b) return a;
  return b;
}

double hypot(double x, double y){
  return sqrt(x * x + y * y);
}

double dist(double x1, double y1, double x2, double y2){
  return hypot(x2 - x1, y2 - y1);
}

double pow(double b, double e){
  #undef pow
  return std::pow(b, e);
  #define pow _pow_aux
}

double sin(double x){
  #undef sin
  return std::sin(x);
  #define sin _sin_aux
}

double cos(double x){
  #undef cos
  return std::cos(x);
  #define cos _cos_aux
}

double log_2(double x){
  return std::log2(x);
}

int read_int(){
  int n;
  std::cin >> n;
  return n;
}

String read_str(){
  String str;
  std::cin >> str;
  return str;
}

std::vector<int> read_int_vec(){
  int len = read_int();
  assert(len >= 0);

  std::vector<int> vec = std::vector<int>(len);

  for(int i = 0; i != len; i++)
    vec.at(i) = read_int();

  return vec;
}

String read_file(const String& pth){
  std::ifstream st {pth};
  std::stringstream buf;
  buf << st.rdbuf();
  st.close();
  
  return buf.str();
}

int parse_hex_digit(char c){
  if(c >= '0' && c <= '9')
    return c - '0';

  if(c >= 'a' && c <= 'z')
    return c - 'a';

  if(c >= 'A' && c <= 'Z')
    return c - 'A';

  assert(0);
  return 0;
}

std::vector<String> sanl(const String& str){
  std::vector<String> xs;
  String s;

  for(char c : str){
    assert(c != '\r');

    if(c == '\n'){
      xs.insert(xs.end(), s);
      s = "";
      continue;
    }

    s += c;
  }

  xs.insert(xs.end(), s);

  return xs;
}

Prop ends_with(const String& s1, const String& s2){
  int len1 = s1.size();
  int len2 = s2.size();

  if(len1 < len2) return 0;

  for(int i = 1; i <= len2; i++)
    if(s1[len1 - i] != s2[len2 - i])
      return 0;
  
  return 1;
}

Prop is_digit(char c){
  return c >= '0' && c <= '9';
}

Prop is_lower_letter(char c){
  return c >= 'a' && c <= 'z';
}

Prop is_upper_letter(char c){
  return c >= 'A' && c <= 'Z';
}

Prop is_letter(char c){
  return is_lower_letter(c) || is_upper_letter(c);
}

Prop is_alphanum(char c){
  return is_letter(c) || is_digit(c);
}

Prop is_whitespace(char c){
  return (0
    || c == '\r'
    || c == '\n'
    || c == '\t'
    || c == ' '
  );
}

int parse_nat(const String& s){
  int n = 0;

  for(char c : s)
    n = n * 10 + (c - '0');

  return n;
}

char digit_to_hex(uint8_t d){
  char c = static_cast<char>(d);
  if(d < 10) return '0' + c;
  return 'A' + (c - 10);
}

String char_to_cc_hex(char c){
  uint8_t n = static_cast<uint8_t>(c);
  
  return String{}
    + digit_to_hex(n >> 4)
    + digit_to_hex(n & 0xF);
}