#include "json.hh"
#include "log.hh"

String ToJson::to_json() const {
  assert(0);
  return "";
}

template<>
String to_json<int>(const int& n){
  return std::to_string(n);
}

template<>
String to_json<String>(const String& str){
  char quote = '"';
  char back_slash = '\\';

  String s {};
  s += quote;

  for(char c : str){
    if(c == quote || c == back_slash){
      s += back_slash;
      s += c;
      continue;
    }
    
    if(c == '\n'){
      s += "\\n";
      continue;
    }
    
    if(c == '\r'){
      s += "\\r";
      continue;
    }
    
    if(c < ' ' || c > '~'){
      s += "\\u00";
      s += char_to_cc_hex(c);
      continue;
    }
    
    s += c;
  }

  s += quote;

  return s;
}

template<>
String to_json<const char*>(const char* const& str){
  return to_json(String{str});
}

template<>
String to_json<ToJson>(const ToJson& obj){
  return obj.to_json();
}