#pragma once

#include "core.hh"

#define EXPR_NIL 0

class Elab {
public:
  static Elab* elab;

  Elab();
  ~Elab();
  int get_ctors_num() const;
  void set_ctors_num(int);
  Expr nat_to_ctor(int);
  Expr ctor(const String&, Prop add_to_mp = 1);
  void init_ctor(const String&);
  Expr param(int);
  Expr param(const String&);
  Expr get_nil();
  Expr mk_un(Expr);
  Expr param_succ(Expr);
  Expr app(Expr, Expr);
  String show(int, Prop raw = 0);
  String show(Expr, Prop raw, int&, int&);
  String show(Expr, Prop raw = 0);
  String show(const String&, Prop raw = 0);
  String show(const std::vector<Expr>&, Prop raw = 0);
  void disp(int, Prop raw = 0);
  void disp(Expr, Prop raw = 0);
  void disp(const String&, Prop raw = 0);
  void disp(const std::vector<Expr>&, Prop raw = 0);
  Expr mk_reduction(Expr, Expr);
  Prop has(const String&) const;
  Prop has(Expr) const;
  Expr get(const String&) const;
  String get(Expr) const;
  void name(const String&, Expr);
  void set_nat(Expr, Expr);
  void clear_nat();
  Expr nat(int);
  void set_list(Expr, Expr);
  void clear_list();
  Expr list(const std::vector<Expr>&);
  void set_set(Expr, Expr);
  void clear_set();
  Expr set(const std::vector<Expr>&);
  Int get_exprs_num();
  Int get_rules_num();

private:
  int ctors_num;
  Prop has_set_nat = 0;
  Prop has_set_list = 0;
  Prop has_set_set = 0;
  Expr nil0;
  Expr nil;
  Expr c_param_zero;
  Expr c_param_succ;
  Expr c_reduces;
  Expr c_alt;
  Expr c_nat_zero;
  Expr c_nat_succ;
  Expr c_list_nil;
  Expr c_list_cons;
  Expr c_set_nil;
  Expr c_set_cons;
  // Set<Expr> show_raw_es;
  Map<Expr, int> mp_e_ctor;
  Map<int, Expr> mp_ctor_e;
  Map<Expr, int> mp_e_param;
  Map<int, Expr> mp_param_e;
  Map<Expr, int> mp_e_nat;
  Map<int, Expr> mp_nat_e;
  Map<String, Expr> mp_name_e;
  Map<Expr, String> mp_e_name;
};