#pragma once

#include "core.hh"
#include "json.hh"

class Parser;

class ExprInfo : public ToJson {
friend class Parser;

public:
  virtual String to_json() const;

private:
  const Parser* par;
  Expr e;
  
  ExprInfo() = delete;
  ExprInfo(const Parser* par, Expr e) :
    par(par), e(e) {};
};